﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Universo91.Aulore.API.Models
{
    public class CollectionItem
    {
        public string nome { get; set; }

        public string colecao { get; set; }

        public Uri imagem { get; set; }

        public int imagem_largura { get; set; }

        public int imagem_altura { get; set; }

        public string descricao { get; set; }
    }
}