﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel;

namespace Universo91.Aulore.API.Models
{
    public class Email
    {
        [DisplayName("nome")]
        [Required(ErrorMessage = "O campo {0} deve ser preenchido")]
        public string nome { get; set; }

        public string email { get; set; }

        public string telefone { get; set; }

        public string cidade { get; set; }

        public string estado { get; set; }

        public string nomejoia { get; set; }

        public string urljoia { get; set; }
    }

    public class resposta
    {
        public string msgretorno { get; set; }
    }
}