﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Universo91.Aulore.API.Models
{
    public class Video
    {
        public string title { get; set; }

        public Uri thumbnail { get; set; }

        public Uri link { get; set; }
    }
}