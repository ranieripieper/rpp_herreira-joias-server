﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Universo91.Aulore.API.Models
{
    /// <summary>
    /// Imagens e Vídeos
    /// </summary>
    public class LookAndPress
    {
        public Uri imagem { get; set; }

        public int imagem_largura { get; set; }

        public int imagem_altura { get; set; }
    }
}