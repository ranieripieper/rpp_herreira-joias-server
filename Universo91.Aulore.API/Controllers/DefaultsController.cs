﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using Universo91.Aulore.API.Data;
using Universo91.Aulore.API.Models;

namespace Universo91.Aulore.API.Controllers
{
    public class DefaultsController : ApiController
    {
        [HttpGet]
        public IList<CollectionItem> Home(int page, int perPage, Guid token)
        {
            using(DBDataAccess db = new DBDataAccess())
            {
                IList<CollectionItem> lista = db.GetAllItemsFromHome(page, perPage, token);

                return lista;
            }
        }

        [HttpGet]
        public IList<CollectionItem> Colecao(int page, int perPage, Guid token, string colecao)
        {
            using(DBDataAccess db = new DBDataAccess())
            {
                colecao = (colecao ?? String.Empty).ToLower().Trim();
                DBDataAccess.CollectionCategory collectionEnum = colecao == "lancamentos" ?
                                                                    DBDataAccess.CollectionCategory.Lancamentos :
                                                                    colecao == "classicas" ?
                                                                        DBDataAccess.CollectionCategory.Classicas :
                                                                        DBDataAccess.CollectionCategory.NaoDefinida;
                IList<CollectionItem> lista = db.GetAllItemsFromCollection(page, perPage, token, collectionEnum);

                return lista;
            }
        }

        [HttpGet]
        public IList<LookAndPress> LookAndPress(int page, int perPage, Guid token)
        {
            using (DBDataAccess db = new DBDataAccess())
            {
                IList<LookAndPress> lista = db.GetAllItemsFromLookAndPress(page, perPage, token);

                return lista;
            }
        }

        [HttpGet]
        public IList<Video> Video(int page, int perPage, Guid token)
        {
            using (DBDataAccess db = new DBDataAccess())
            {
                IList<Video> lista = db.GetAllItemsFromVideos(page, perPage, token);

                return lista;
            }
        }

        [HttpPost]
        public IList<resposta> SendEmail(Guid token, Email dados)
        {
            using (DBDataAccess db = new DBDataAccess())
            {
                IList<resposta> resposta = db.SendFormEmail(token, dados);

                return resposta;
            }           
        }
    }
}
