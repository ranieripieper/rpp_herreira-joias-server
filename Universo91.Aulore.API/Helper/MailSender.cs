﻿using System;
using System.IO;
using System.Text;
using System.Web;
using System.Collections;
using NetMail = System.Net.Mail;
using WebMail = System.Web.Mail;
using System.Net;
using System.Configuration;
using CDO;

namespace Universo91.Aulore.API.Helper
{
    public class MailSender
    {
        Hashtable parametrosDeSubstituicao = new Hashtable();

        public MailSender() { }

        public void Send(string body)
        {
            string to = System.Configuration.ConfigurationManager.AppSettings["EmailTo"].ToString();
            string subject = System.Configuration.ConfigurationManager.AppSettings["EmailSubject"].ToString();
            this.SendCDOMessage(to, subject, body);
        }

        /// <summary>
        /// Send an electronic message using the Collaboration Data Objects (CDO).
        /// </summary>
        /// <remarks>http://support.microsoft.com/kb/310212</remarks>
        private void SendCDOMessage(string to, string subject, string body)
        {
            try
            {
                CDO.Message message = new CDO.Message();
                CDO.IConfiguration configuration = message.Configuration;
                ADODB.Fields fields = configuration.Fields;

                // Set configuration.
                // sendusing:               cdoSendUsingPort, value 2, for sending the message using the network.
                // smtpauthenticate:     Specifies the mechanism used when authenticating to an SMTP service over the network.
                //                                  Possible values are:
                //                                  - cdoAnonymous, value 0. Do not authenticate.
                //                                  - cdoBasic, value 1. Use basic clear-text authentication. (Hint: This requires the use of "sendusername" and "sendpassword" fields)
                //                                  - cdoNTLM, value 2. The current process security context is used to authenticate with the service.

                ADODB.Field field = fields["http://schemas.microsoft.com/cdo/configuration/smtpserver"];
                field.Value = System.Configuration.ConfigurationManager.AppSettings["EmailServer"].ToString();

                field = fields["http://schemas.microsoft.com/cdo/configuration/smtpserverport"];
                field.Value = System.Configuration.ConfigurationManager.AppSettings["EmailPort"].ToString();

                field = fields["http://schemas.microsoft.com/cdo/configuration/sendusing"];
                field.Value = CDO.CdoSendUsing.cdoSendUsingPort;

                field = fields["http://schemas.microsoft.com/cdo/configuration/smtpauthenticate"];
                field.Value = CDO.CdoProtocolsAuthentication.cdoBasic;

                field = fields["http://schemas.microsoft.com/cdo/configuration/sendusername"];
                field.Value = System.Configuration.ConfigurationManager.AppSettings["EmailUserName"].ToString();

                field = fields["http://schemas.microsoft.com/cdo/configuration/sendpassword"];
                field.Value = System.Configuration.ConfigurationManager.AppSettings["EmailPassword"].ToString();

                field = fields["http://schemas.microsoft.com/cdo/configuration/smtpusessl"];
                field.Value = System.Configuration.ConfigurationManager.AppSettings["EmailUseSSL"].ToString();

                fields.Update();

                message.From = System.Configuration.ConfigurationManager.AppSettings["EmailFrom"].ToString();
                message.To = to;
                message.Subject = subject;
                message.TextBody = body;

                // Send message.
                message.Send();

            }
            catch (Exception ex)
            {
                HttpContext.Current.Trace.Write("Error when sending the e-mail: \n\n" + body);
                throw ex;
            }
        }



        public void Send(string to, string subject, string body)
        {
            try
            {
                NetMail.MailMessage email = new NetMail.MailMessage();
                email.To.Add(new NetMail.MailAddress(to));
                email.Subject = subject;
                email.BodyEncoding = Encoding.UTF8;
                email.IsBodyHtml = true;
                email.Body = body;

                NetMail.SmtpClient _smtp = new NetMail.SmtpClient();

                #region SMTP
                /*
				bool useSMTP = Convert.ToBoolean(ConfigurationManager.AppSettings["useSMTP"]);

				if (useSMTP)
				{
					_smtp.Host = ConfigurationManager.AppSettings["SMTPHost"];
					_smtp.Port = int.Parse(ConfigurationManager.AppSettings["SMTPPort"]);
					_smtp.EnableSsl = false;

					string pass = ConfigurationManager.AppSettings["SMTPPass"];
					string user = ConfigurationManager.AppSettings["SMTPUser"];

					if (!string.IsNullOrEmpty(pass) && !string.IsNullOrEmpty(user))
					{
						_smtp.UseDefaultCredentials = false;
						NetworkCredential basicCredential = new NetworkCredential(user, pass);
						_smtp.Credentials = basicCredential;
					}
					else
					{
						_smtp.UseDefaultCredentials = true;
					}
				}
				*/
                #endregion

                _smtp.Send(email);
            }
            catch (Exception ex)
            {
                HttpContext.Current.Trace.Write("Error when sending the e-mail: \n\n" + body);
                throw ex;
            }
        }

        public void SendHtmlBody(string to, string subject, string arqBody)
        {
            try
            {
                System.IO.StreamReader oRead = new System.IO.StreamReader(Path.Combine(HttpContext.Current.Server.MapPath("~/App_Data/email/" + arqBody)), System.Text.Encoding.UTF8);

                // Get Body
                string body = string.Empty;
                body = oRead.ReadToEnd();
                oRead.Close();

                // REPLACE E-MAIL TAGS
                foreach (DictionaryEntry valor in parametrosDeSubstituicao)
                {
                    body = body.Replace(valor.Key.ToString(), valor.Value.ToString());
                    //subject = subject.Replace(valor.Key.ToString(), valor.Value.ToString());
                }

                Send(to, subject, body);
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public Hashtable ParametrosDeSubstituicao
        {
            get
            {
                return parametrosDeSubstituicao;
            }
            set
            {
                parametrosDeSubstituicao = value;
            }
        }

    }
}