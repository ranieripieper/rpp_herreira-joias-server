﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text.RegularExpressions;
using System.Web;
using MySql.Data.MySqlClient;
using Universo91.Aulore.API.Models;
using Universo91.Aulore.API.Helper;
using System.Text;

namespace Universo91.Aulore.API.Data
{
    public class DBDataAccess : IDisposable
    {
        #region Fields
        private MySqlConnection _conn;
        private MySqlCommand _cmd;
        private string _sqlHome = String.Concat("select p.id, p.post_title, t.name, pp.guid, p.post_content, pm.meta_value ",
                                                "from wp_posts p ",
                                                "inner join wp_posts pp on pp.post_type = 'attachment' and p.id = pp.post_parent ",
                                                "inner join wp_term_relationships tr on p.id = tr.object_id ",
                                                "inner join wp_terms t on tr.term_taxonomy_id = t.term_id ",
                                                "inner join wp_postmeta pm on pp.id = pm.post_id and meta_key = '_wp_attachment_metadata' ",
                                                "where t.term_id in (5, 6, 7);");
        private string _sqlCollections = String.Concat("select p.id, p.post_title, t.name, pp.guid, p.post_content, pm.meta_value ",
                                                       "from wp_posts p ",
                                                       "inner join wp_posts pp on pp.post_type = 'attachment' and p.id = pp.post_parent ",
                                                       "inner join wp_term_relationships tr on p.id = tr.object_id ",
                                                       "inner join wp_terms t on tr.term_taxonomy_id = t.term_id ",
                                                       "inner join wp_postmeta pm on pp.id = pm.post_id and meta_key = '_wp_attachment_metadata' ",
                                                       "where t.term_id = {0};");
        private string _sqlLookAndPress = String.Concat("select p.id, pp.guid, pm.meta_value ",
                                                        "from wp_posts p ",
                                                        "inner join wp_posts pp on pp.post_type = 'attachment' and p.id = pp.post_parent ",
                                                        "inner join wp_term_relationships tr on p.id = tr.object_id ",
                                                        "inner join wp_terms t on tr.term_taxonomy_id = t.term_id ",
                                                        "inner join wp_postmeta pm on pp.id = pm.post_id and meta_key = '_wp_attachment_metadata' ",
                                                        "where t.term_id = 6;");
        private string _sqlVideos = String.Concat("select p.id, p.post_title, pp.guid ",// pm.meta_value ",
                                                        "from wp_posts p ",
                                                        "inner join wp_posts pp on pp.post_type = 'attachment' and p.id = pp.post_parent ",
                                                        "inner join wp_term_relationships tr on p.id = tr.object_id ",
                                                        "inner join wp_terms t on tr.term_taxonomy_id = t.term_id ",
                                                        //"inner join wp_postmeta pm on pp.id = pm.post_id and meta_key = '_wp_attachment_metadata' ",
                                                        "where pp.guid like '%.mp4';");
        #endregion

        public enum CollectionCategory { Lancamentos, Classicas, NaoDefinida };

        public DBDataAccess()
        {
            string cs = System.Configuration.ConfigurationManager.ConnectionStrings["MySQLCS"].ConnectionString;
            this._conn = new MySqlConnection(cs);
            this._conn.Open();
        }
        public void Dispose()
        {
            if (this._conn != null)
            {
                if (this._conn.State != System.Data.ConnectionState.Closed)
                    this._conn.Clone();
                this._conn.Dispose();
            }
        }

        private void CalculateIndexes(int currentPage, int recordsPerPage, out int firstIndex, out int lastIndex)
        {
            firstIndex = (currentPage * recordsPerPage) - recordsPerPage;
            lastIndex = (firstIndex + recordsPerPage) - 1;
        }

        private bool IsThisTokenValid(Guid token)
        {
            bool result = false;
            int rows = 0;
            /// todo: corrigir sql para verificar token
            this._cmd = new MySqlCommand(String.Format("select count(umeta_id) from wp_usermeta where meta_key = 'token' and meta_value = '{0}'", token.ToString()), this._conn);

            if (int.TryParse((this._cmd.ExecuteScalar() ?? new object()).ToString(), out rows) && rows > 0)
                result = true;

            return result;
        }

        #region Converters
        private string _patternTag = @"<([a-z]+)([^<]+)*(?:>(.*)<\/\1>|\s+\/>)",
                       _patternSlideShow = @"(\[slideshow\][\n|\r]*.*[\n|\r]*\[\/slideshow\][\n|\r]*)";
        private CollectionItem ConvertToCollectionItem(MySqlDataReader dr)
        {
            CollectionItem result = null;

            if(dr != null)
            {
                string metaValue = dr["meta_value"].ToString(),
                       postContent = dr["post_content"].ToString();
                int height, width;
                this.GetWidthAndHeightFromMetaValue(metaValue, out width, out height);

                result = new CollectionItem();
                result.nome = dr["post_title"].ToString();
                result.colecao = dr["name"].ToString();
                result.imagem = new Uri(dr["guid"].ToString(), UriKind.Absolute);
                result.imagem_largura = width;
                result.imagem_altura = height;

                if (Regex.Match(postContent, _patternSlideShow).Success)
                    result.descricao = Regex.Replace(postContent, _patternSlideShow, String.Empty);
                else if (Regex.Match(postContent, _patternTag).Success)
                    result.descricao = Regex.Replace(postContent, _patternTag, String.Empty);
            }

            return result;
            
        }

        private Video ConvertToVideo(MySqlDataReader dr)
        {
            Video result = null;

            if (dr != null)
            {
                //string metaValue = dr["meta_value"].ToString();

                result = new Video();
                result.title = dr["post_title"].ToString();
                //result.thumbnail = this.GetThumbnailFromMetaValue(metaValue);
                result.link = new Uri(dr["guid"].ToString(), UriKind.Absolute);
            }

            return result;
        }

        private LookAndPress ConvertToLookAndPress(MySqlDataReader dr)
        {
            LookAndPress result = null;

            if (dr != null)
            {
                string metaValue = dr["meta_value"].ToString();
                int height, width;
                this.GetWidthAndHeightFromMetaValue(metaValue, out width, out height);

                result = new LookAndPress();
                result.imagem = new Uri(dr["guid"].ToString(), UriKind.Absolute);
                result.imagem_largura = width;
                result.imagem_altura = height;
            }

            return result;
        }

        private void GetWidthAndHeightFromMetaValue(string metaValue, out int width, out int height)
        {
            height = width = 0;

            string patternWidth = @"([\w]{1}\:[0-9]+\:\""width\""\;[\w]{1})\:([0-9]+)\;", patternHeight = @"([\w]{1}\:[0-9]+\:\""height\""\;[\w]{1})\:([0-9]+)\;";
            Match mWidth = Regex.Match(metaValue, patternWidth),
                  mHeight = Regex.Match(metaValue, patternHeight);

            if(mWidth != null && mWidth.Groups != null && mWidth.Groups.Count > 1)
                int.TryParse(mWidth.Groups[2].Value, out width);

            if (mHeight != null && mHeight.Groups != null && mHeight.Groups.Count > 1)
                int.TryParse(mHeight.Groups[2].Value, out height);
        }

        private Uri GetThumbnailFromMetaValue(string metaValue)
        {
            Uri url = null;

            return url;
        }
        #endregion


        public IList<CollectionItem> GetAllItemsFromHome(int page, int perPage, Guid token)
        {
            IList<CollectionItem> result = new List<CollectionItem>();

            try
            {
                if(this.IsThisTokenValid(token))
                {
                    this._cmd = this._conn.CreateCommand();
                    this._cmd.CommandText = this._sqlHome;

                    MySqlDataReader dr = this._cmd.ExecuteReader();
                    int index = 0, firstIndex, lastIndex;
                    this.CalculateIndexes(page, perPage, out firstIndex, out lastIndex);

                    while(dr.Read())
                    {
                        if(index >= firstIndex && index <= lastIndex)
                            result.Add(this.ConvertToCollectionItem(dr));

                        index++;

                        if (index > lastIndex)
                            break;
                    }
                }
            }
            catch
            {
                result = new List<CollectionItem>();
            }

            return result;
        }

        internal IList<CollectionItem> GetAllItemsFromCollection(int page, int perPage, Guid token, CollectionCategory collectionCategory)
        {
            IList<CollectionItem> result = new List<CollectionItem>();
            try
            {
                if (this.IsThisTokenValid(token) && collectionCategory != CollectionCategory.NaoDefinida)
                {
                    this._cmd = this._conn.CreateCommand();
                    this._cmd.CommandText = String.Format(this._sqlCollections, collectionCategory == CollectionCategory.Lancamentos ? 5 : 7);

                    MySqlDataReader dr = this._cmd.ExecuteReader();
                    int index = 0, firstIndex, lastIndex;
                    this.CalculateIndexes(page, perPage, out firstIndex, out lastIndex);

                    while (dr.Read())
                    {
                        if (index >= firstIndex && index <= lastIndex)
                            result.Add(this.ConvertToCollectionItem(dr));

                        index++;

                        if (index > lastIndex)
                            break;
                    }
                }
            }
            catch
            {
                result = new List<CollectionItem>();
            }

            return result;
        }

        public IList<LookAndPress> GetAllItemsFromLookAndPress(int page, int perPage, Guid token)
        {
            IList<LookAndPress> result = new List<LookAndPress>();

            try
            {
                if (this.IsThisTokenValid(token))
                {
                    this._cmd = this._conn.CreateCommand();
                    this._cmd.CommandText = this._sqlLookAndPress;

                    MySqlDataReader dr = this._cmd.ExecuteReader();
                    int index = 0, firstIndex, lastIndex;
                    this.CalculateIndexes(page, perPage, out firstIndex, out lastIndex);

                    while (dr.Read())
                    {
                        if (index >= firstIndex && index <= lastIndex)
                            result.Add(this.ConvertToLookAndPress(dr));

                        index++;

                        if (index > lastIndex)
                            break;
                    }
                }
            }
            catch
            {
                result = new List<LookAndPress>();
            }

            return result;
        }

        public IList<Video> GetAllItemsFromVideos(int page, int perPage, Guid token)
        {
            IList<Video> result = new List<Video>();

            try
            {
                if (this.IsThisTokenValid(token))
                {
                    this._cmd = this._conn.CreateCommand();
                    this._cmd.CommandText = this._sqlVideos;

                    MySqlDataReader dr = this._cmd.ExecuteReader();
                    int index = 0, firstIndex, lastIndex;
                    this.CalculateIndexes(page, perPage, out firstIndex, out lastIndex);

                    while (dr.Read())
                    {
                        if (index >= firstIndex && index <= lastIndex)
                            result.Add(this.ConvertToVideo(dr));

                        index++;

                        if (index > lastIndex)
                            break;
                    }
                }
            }
            catch
            {
                result = new List<Video>();
            }

            return result;
        }

        public IList<resposta> SendFormEmail(Guid token, Email dados)
        {
            IList<resposta> _ret = new List<resposta>();
            resposta _resp = new resposta();

            StringBuilder _msg = new StringBuilder();

            try
            {
                bool _useToken = System.Configuration.ConfigurationManager.AppSettings["EmailServer"].ToString() == "true";
                if ((!_useToken) || (this.IsThisTokenValid(token)))
                {
                    if (!notFilledEmail(dados))
                    {
                        #region Message
                        _msg.AppendLine("Formulário de email");
                        _msg.AppendLine("");
                        _msg.AppendLine("");
                        _msg.AppendLine("Dados");
                        _msg.AppendLine("");
                        _msg.AppendLine(string.Format("Nome: {0}", dados.nome));
                        _msg.AppendLine("");
                        _msg.AppendLine(string.Format("Email: {0}", dados.email));
                        _msg.AppendLine("");
                        _msg.AppendLine(string.Format("Telefone: {0}", dados.telefone));
                        _msg.AppendLine("");
                        _msg.AppendLine(string.Format("Cidade: {0}", dados.cidade));
                        _msg.AppendLine("");
                        _msg.AppendLine(string.Format("Estado: {0}", dados.estado));
                        _msg.AppendLine("");
                        _msg.AppendLine(string.Format("Nome da jóia: {0}", dados.nomejoia));
                        _msg.AppendLine("");
                        _msg.AppendLine(string.Format("URL da imagem da jóia: {0}", dados.urljoia));
                        _msg.AppendLine("");
                        #endregion

                        MailSender _mail = new MailSender();
                        _mail.Send(_msg.ToString());
                        _resp.msgretorno = "OK";
                    }
                    else throw (new Exception("Falta(m) campo(s) obrigatorio(s)"));
                }
                else
                    throw (new Exception("Problemas no token"));
            }
            catch (Exception ex)
            {
                _resp.msgretorno = ex.Message;
            }
 
            _ret.Add(_resp);

            return _ret;
        }
        private bool notFilledEmail(Email dados)
        {
            return string.IsNullOrEmpty(dados.nome) || string.IsNullOrEmpty(dados.email) || string.IsNullOrEmpty(dados.nomejoia) || string.IsNullOrEmpty(dados.urljoia);
        }
    }
}