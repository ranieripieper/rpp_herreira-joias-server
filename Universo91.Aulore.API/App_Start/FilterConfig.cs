﻿using System.Web;
using System.Web.Mvc;

namespace Universo91.Aulore.API
{
    public class FilterConfig
    {
        public static void RegisterGlobalFilters(GlobalFilterCollection filters)
        {
            filters.Add(new HandleErrorAttribute());
        }
    }
}