﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web.Http;

namespace Universo91.Aulore.API
{
    public static class WebApiConfig
    {
        public static void Register(HttpConfiguration config)
        {

            config.Routes.MapHttpRoute(
                name: "Email",
                routeTemplate: "SendEmail.json/{token}",
                defaults: new
                {
                    controller = "Defaults",
                    action = "SendEmail",
                    token = Guid.Empty
                }
            );

            config.Routes.MapHttpRoute(
                name: "Home",
                routeTemplate: "{action}.json/{page}/{perPage}/{token}/{colecao}",
                defaults: new
                {
                    controller = "Defaults",
                    action = "Colecao",
                    page = 1,
                    perPage = 20,
                    token = Guid.Empty,
                    colecao = String.Empty
                }
            );

            // Uncomment the following line of code to enable query support for actions with an IQueryable or IQueryable<T> return type.
            // To avoid processing unexpected or malicious queries, use the validation settings on QueryableAttribute to validate incoming queries.
            // For more information, visit http://go.microsoft.com/fwlink/?LinkId=279712.
            //config.EnableQuerySupport();

            // To disable tracing in your application, please comment out or remove the following line of code
            // For more information, refer to: http://www.asp.net/web-api
            config.EnableSystemDiagnosticsTracing();
            config.Formatters.Remove(config.Formatters.XmlFormatter);
        }
    }
}
